<?php

namespace App\Lib;

use DOMPDF;

define('DOMPDF_ENABLE_REMOTE', true);
//require_once("dompdf/dompdf_config.inc.php");

class myPdf{
    public function render($html, $nombre){
        header('Content-type: application/pdf');
        header('Content-Disposition: attachment; filename='.$nombre.'.pdf"');
        
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($nombre, array('compress'=>1, 'Attachment' => 0));
    }
    
    public function renderToFile($html, $nombre){
        $dompdf = new \Dompdf\Dompdf();
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        \Storage::delete($nombre);
        \Storage::put($nombre, $output);
    }
}