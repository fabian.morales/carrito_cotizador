<?php

namespace App\Helpers;

class Helper
{
    
    public static function cargueInicial($seccion=""){
        
    }
    
    public static function number_format($value)
    {
        return number_format((float)$value, 0, ",", ".");
    }
        
    public static function validarCaptcha($response, $remote){
        //$secret = "6Lec9RgUAAAAAMY9VuUJgRc51dvh7PErw5_aAuDH";
        
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $fields = [
            'secret' => '6Lec9RgUAAAAAMY9VuUJgRc51dvh7PErw5_aAuDH',
            'response' => $response,
            'remoteip' => $remote
        ];

        $fields_string = '';
        foreach($fields as $key=>$value) {
            $fields_string .= $key.'='.$value.'&'; 
        }

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);
        
        $retorno = false;
        if (isset($result->success) && $result->success){
            $retorno = true;
        }

        return $retorno;

    }
}